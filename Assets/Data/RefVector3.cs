﻿using UnityEngine;

[CreateAssetMenu]
public class RefVector3 : ScriptableObject
{

    public Vector3 Data;

}
