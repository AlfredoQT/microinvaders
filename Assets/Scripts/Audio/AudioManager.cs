﻿using System;
using UnityEngine;

public class AudioManager : MonoBehaviour
{

	#region GlobalVariables

		[SerializeField]
		internal Sound[] Sounds;

		public static AudioManager Instance;

	#endregion

	#region AudioSettings

		private void Awake()
		{
			if (Instance ==  null)
			{
				Instance = this;
				Init();
				
			}
			else
			{
				Destroy(gameObject);
				return;
			}
			// Persistent
			DontDestroyOnLoad(gameObject);
		}

		private void Init()
		{
			foreach(Sound sound in Sounds)
			{
				sound.source = gameObject.AddComponent<AudioSource>();
				sound.source.clip = sound.clip;
				sound.source.volume = sound.volume;
				sound.source.pitch = sound.pitch;
				sound.source.loop = sound.loop;
				sound.source.spatialBlend = sound.SpatialBlend;
			}
		}

		public void PlaySound(string name)
		{
			Sound sound = LookForSound(name);
			if (sound != null)
			{
				sound.source.Play();
			}
			else
			{
				print(name);
			}
			
		}

		public void StopSound(string name)
		{
			Sound sound = LookForSound(name);
			if (sound != null)
			{
				sound.source.Stop();
			}
		}

		public void StopAllSounds()
		{
			foreach(Sound sound in Sounds)
			{
				if (sound != null)
				{
					sound.source.Stop();
				}
			}
		}

		private Sound LookForSound(string name)
		{
			foreach(Sound sound in Sounds)
			{
				if (String.Compare(name, sound.name) == 0)
				{
					return sound;
				}
			}
			return null;
		}

		public float GetSoundLength(string name)
		{
			Sound sound = LookForSound(name);
			return sound.clip.length;
		}

    #endregion

}
