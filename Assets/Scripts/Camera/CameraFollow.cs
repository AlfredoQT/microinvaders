﻿using UnityEngine;

[System.Serializable]
public struct MinMaxClamp
{
	public Vector2 Min;
	public Vector2 Max;
}

[RequireComponent(typeof(Camera))]
public class CameraFollow : MonoBehaviour
{

	#region GlobalVars

		[SerializeField]
		private Transform _Target;

		// Offset on height to move camera to best position.
		[SerializeField]
		internal Vector3 CameraOffset;

		// Cause delay when updating the camera position to make it feel smoother
		[Range(0.0f, 1.0f)]
		[SerializeField]
		internal float Smoothing = 0.05f;

		// Values to clamp the camera to
		[SerializeField]
		internal MinMaxClamp ClampParameters;

		private Camera _Cam;

		private Vector2 _Extents;

	#endregion

	#region LifeCycle

		private void Awake()
		{
			_Cam = GetComponent<Camera>();
		}

		private void Update()
		{
			_Extents.y = _Cam.orthographicSize;
			_Extents.x = _Extents.y * Screen.width / Screen.height;
		}

		// The target is a rigid body. We need to make sure the camera position is updated at the same time
		// physics are updated
		private void FixedUpdate()
		{
			Vector3 newPosition = _Target.position;
			newPosition += CameraOffset; 

			// Clamping
			newPosition = new Vector3(
				Mathf.Clamp(newPosition.x, ClampParameters.Min.x + _Extents.x, ClampParameters.Max.x - _Extents.x),
				Mathf.Clamp(newPosition.y, ClampParameters.Min.y + _Extents.y, ClampParameters.Max.y - _Extents.y),
				-10.0f
			);

			// print("Current position: " + transform.position.ToString());
			// print("New position: " + newPosition.ToString());

			// We want the smoothing to be frame independent. Its always the same this way
			transform.position = Vector3.Lerp(transform.position, newPosition, Smoothing);
		}

	#endregion
}
