﻿using System;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Rigidbody2D))]
public class BlobEnemy : Enemy, IFreeze
{

	#region GlobalVars

		[SerializeField]
		private float _MoveSpeed = 8.0f;

		[SerializeField]
		private float _RaycastDistance = 2.0f;

		[SerializeField]
		private LayerMask _RaycastLayer;

		[SerializeField]
		private float _DamageDealt = 20.0f;

		private SpriteRenderer _SR;
		private Rigidbody2D _RB;
		private Animator _Anim;

		private float _Direction = -1.0f;

	#endregion

	#region LifeCycle

		private void Awake()
		{
			_SR = GetComponent<SpriteRenderer>();
			_RB = GetComponent<Rigidbody2D>();
			_Anim = GetComponent<Animator>();
		}

		private void Start()
		{
			Vector3 toTarget = _Target.position - transform.position;
			_Direction = toTarget.x > 0.0f ? 1.0f : -1.0f;
			AudioManager.Instance.PlaySound("BlobSpawn");
		}

		private void Update()
		{
			_SR.flipX = _Direction > 0.0f ? true : (_Direction < 0.0f ? false : _SR.flipX);
			_RB.velocity = new Vector2(_Direction * _MoveSpeed, _RB.velocity.y);

			ChangeDirection();
		}

    #endregion

	#region Collisions

		private void ChangeDirection()
		{
			RaycastHit2D hit = Physics2D.Raycast(transform.position, transform.right * _Direction, _RaycastDistance, _RaycastLayer);
			Debug.DrawRay(transform.position, transform.right * _Direction * _RaycastDistance);
			Collider2D collider = hit.collider;
			if (collider)
			{
				_Direction = -_Direction;
			}
		}

		private void OnCollisionStay2D(Collision2D collision)
		{
			if (collision.collider.CompareTag("Player"))
			{
				Health playerHealth = collision.collider.GetComponent<Health>();
				if (playerHealth !=  null)
				{
					playerHealth.ReceiveDamage(_DamageDealt);
				}
			}
		}

		public void Freeze()
		{
			_RB.constraints = RigidbodyConstraints2D.FreezePositionX;
			_SR.color = new Color(113.0f / 255.0f, 124.0f / 255.0f, 227.0f / 255.0f);
			_Anim.speed = 0.0f;
		}

		public void UnFreeze()
		{
			_RB.constraints = RigidbodyConstraints2D.None | RigidbodyConstraints2D.FreezeRotation;
			_SR.color = new Color(1.0f, 1.0f, 1.0f);
			_Anim.speed = 1.0f;
		}

    #endregion
}
