﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class WallBoss : MonoBehaviour
{

	#region Members

		[SerializeField]
		private WallBossEye _EyeOne;

		[SerializeField]
		private WallBossEye _EyeTwo;

		[SerializeField]
		private WallBossCavity _CavityInitial;

		[SerializeField]
		private WallBossCavity _CavityFinal;

		[SerializeField]
		private float _EnemyDetectionRadius = 20.0f;

		[SerializeField]
		private LayerMask _DetectionLayer;

		private FiniteStateMachine _FSM;
		private Animator _Anim;

	#endregion

	#region LifeCycle

		private void Awake()
		{
			_FSM = gameObject.AddComponent(typeof(FiniteStateMachine)) as FiniteStateMachine;
			_Anim = GetComponent<Animator>();
		}

		private void Start()
		{
			_FSM.SetState(Rest());
		}

	#endregion

	#region States

		private IEnumerator Rest()
		{
			while(!TargetInRadius(_EnemyDetectionRadius))
			{
				yield return null;
			}
			_FSM.SetState(Awaken());
		}

		private IEnumerator Awaken()
		{
			_EyeOne.gameObject.SetActive(true);
			_EyeTwo.gameObject.SetActive(true);
			_CavityInitial.gameObject.SetActive(true);
			AudioManager.Instance.PlaySound("");
			while(!ShouldBeMad())
			{
				yield return null;
			}
			_FSM.SetState(Mad());
		}

		private IEnumerator Mad()
		{
			_CavityFinal.gameObject.SetActive(true);
			_Anim.SetBool("ShowCavity", true);
			while(!ShouldDie())
			{
				yield return null;
			}
			AudioManager.Instance.PlaySound("BossDeath");
			EventManager.TriggerEvent("PlayerFrozen");
			EventManager.TriggerEvent("BossDefeated");
		}

		private void OnDrawGizmos()
		{
			Gizmos.DrawWireSphere(transform.position, _EnemyDetectionRadius);
		}

		private bool TargetInRadius(float pRadius)
		{
			Collider2D collider = Physics2D.OverlapCircle(transform.position, pRadius, _DetectionLayer);
			if (collider != null)
			{
				return collider.CompareTag("Player");
			}
			return false;
		}

		private bool ShouldBeMad()
		{
			return _EyeOne == null && _EyeTwo == null && _CavityInitial == null;
		}

		private bool ShouldDie()
		{
			return _CavityFinal == null;
		}

	#endregion

}
