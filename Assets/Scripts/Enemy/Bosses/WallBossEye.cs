﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class WallBossEye : MonoBehaviour
{

	#region EyeTransformModifiers

		[SerializeField, Tooltip("The eye transform will be rotated up to this point")]
		private float _MaxAngle = 30.0f;

		[SerializeField, Tooltip("The eye transform will be rotated up to this point")]
		private float _MinAngle = -30.0f;

		[SerializeField, Tooltip("How many angles per second?")]
		private float _RotationSpeed = 5f;

		[SerializeField]
		private float _MovementSpeed = 8.0f;

		[SerializeField]
		private float _MaxYRelativePosition = 1.9f;

		[SerializeField]
		private float _MinYRelativePosition = -8.03f;

		private Rigidbody2D _RB;
		private float _MovementDirection = 1.0f;
		private float _RotationDirection = 1.0f;
		private float _CurrentAngle = 0.0f;

	#endregion

	#region LifeCycle

		private void Awake()
		{
			_RB = GetComponent<Rigidbody2D>();
		}

		private void Start()
		{
			_RB.gravityScale = 0.0f;
			_CurrentAngle = transform.localEulerAngles.z;
		}

		private void Update()
		{
			if (_CurrentAngle >= _MaxAngle)
			{
				_RotationDirection = -1.0f;
			}
			else if (_CurrentAngle <= _MinAngle)
			{
				_RotationDirection = 1.0f;
			}
			_CurrentAngle += (_RotationSpeed * Time.deltaTime) * _RotationDirection;
			transform.localEulerAngles = new Vector3(0.0f, 0.0f, _CurrentAngle + 360.0f);
		}

		private void FixedUpdate()
		{
			// Check for boundaries
			if (transform.localPosition.y >= _MaxYRelativePosition)
			{
				_MovementDirection = -1.0f;
			}
			else if (transform.localPosition.y <= _MinYRelativePosition)
			{
				_MovementDirection = 1.0f;
			}
			// Move it up and down
			_RB.velocity = new Vector2(0.0f, _MovementSpeed * _MovementDirection);
		}

	#endregion

}
