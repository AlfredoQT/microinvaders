﻿using UnityEngine;

public abstract class Enemy : MonoBehaviour
{
	[SerializeField, Tooltip("This is set by the spawner, but just in case you need it")]
	protected Transform _Target;

	public Transform Target
	{
		get
		{
			return _Target;
		}
		set 
		{
			_Target = value;
		}
	}

}
