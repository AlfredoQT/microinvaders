﻿using UnityEngine;

[System.Serializable]
public struct EnemySpawnData
{
	public Enemy _EnemyType;
	public Transform _SpawnTransform;
}

public class EnemySpawner : MonoBehaviour
{

	#region SpawnData

		[SerializeField, Tooltip("Put the player pretty much")]
		private Transform _EnemiesTarget;

		[SerializeField, Tooltip("Whether the spawner will spawn continuously, the same ones in the array will be spawned")]
		private bool _IsContinuous = false;

		[SerializeField, Tooltip("It does not make any sense to put this if it is not continuous")]
		private float _MinSpawnDelay = 2.0f;

		[SerializeField, Tooltip("It does not make any sense to put this if it is not continuous")]
		private float _MaxSpawnDelay = 2.0f;

		[SerializeField]
		private EnemySpawnData[] _Data;

		private float _TimeLeftToSpawn;

	#endregion

	#region LifeCycle

		private void OnEnable()
		{
			// This time is not 0, because the first time its triggered by the volume
			_TimeLeftToSpawn = Random.Range(_MinSpawnDelay, _MaxSpawnDelay);
			SpawnEnemies();
		}

	#endregion

		private void Update()
		{
			if (_IsContinuous && _TimeLeftToSpawn <= 0.0f)
			{
				SpawnEnemies(); 
				_TimeLeftToSpawn = Random.Range(_MinSpawnDelay, _MaxSpawnDelay);
			}

			_TimeLeftToSpawn = _TimeLeftToSpawn > 0.0f ? _TimeLeftToSpawn - Time.deltaTime : 0.0f;
		}

	#region ClassMethods

		// It can be used both by this class and any triggers
		public void SpawnEnemies()
		{
			for (int i = 0; i < _Data.Length; ++i)
			{
				Enemy enemy = Instantiate(_Data[i]._EnemyType, _Data[i]._SpawnTransform.position, Quaternion.identity);
				enemy.Target = _EnemiesTarget;
			}
		}

	#endregion

}
