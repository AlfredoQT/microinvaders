﻿using System;
using UnityEngine;

[RequireComponent(typeof(Health))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(CapsuleCollider2D))]
[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Animator))]
public class FastMovingEnemy : Enemy, IFreeze
{
	#region GlobalVariables

		[SerializeField]
		private float _MoveSpeed = 8.0f;

		[SerializeField]
		private float _RaycastDistance = 2.0f;

		[SerializeField]
		private LayerMask _RaycastLayer;

		[SerializeField]
		private float _DamageDealt = 20.0f;

		private float _Direction = -1.0f;

		private Health _Health;
		private Rigidbody2D _RB;
		private CapsuleCollider2D _Collider;
		private SpriteRenderer _SR;
		private Animator _Anim;

	#endregion

	#region UnittMethods

		private void Awake()
		{
			_Health = GetComponent<Health>();
			_Collider = GetComponent<CapsuleCollider2D>();
			_RB = GetComponent<Rigidbody2D>();
			_SR = GetComponent<SpriteRenderer>();
			_Anim = GetComponent<Animator>();
		}

		private void Start()
		{
			Vector3 toTarget = _Target.position - transform.position;
			_Direction = toTarget.x > 0.0f ? 1.0f : -1.0f;

			// He is just going to die anyways
			_SR.flipX = _Direction > 0.0f ? true : (_Direction < 0.0f ? false : _SR.flipX);
		}

		private void Update()
		{
			if (_Health.Dead)
			{
				HandleDeath();
				return;
			}
			CheckRaycast();
			_RB.velocity = new Vector2(_Direction * _MoveSpeed, _RB.velocity.y);
		}

    #endregion

    #region ClassMethods

		private void CheckRaycast()
		{
			RaycastHit2D hit = Physics2D.Raycast(transform.position + Vector3.up * 2.0f, transform.right * _Direction, _RaycastDistance, _RaycastLayer);
			Debug.DrawRay(transform.position + Vector3.up * 2.0f, transform.right * _Direction * _RaycastDistance);
			Collider2D collider = hit.collider;
			if (collider)
			{
				Health playerHealth = collider.GetComponent<Health>();
				if (playerHealth != null)
				{
					playerHealth.ReceiveDamage(_DamageDealt);
				}
				_Health.Die();
			}
		}

		private void OnCollisionEnter2D(Collision2D other)
		{
			if (other.collider.CompareTag("Player"))
			{
				Health playerHealth = other.collider.GetComponent<Health>();
				if (playerHealth != null)
				{
					playerHealth.ReceiveDamage(_DamageDealt);
				}
			}
		}

    	private void HandleDeath()
		{
			_Collider.offset = new Vector2(0f, 0.42f);
			_Collider.size = new Vector2(4.31f, 1.13f);
			_Collider.direction = CapsuleDirection2D.Horizontal;
			_RB.constraints = RigidbodyConstraints2D.FreezeAll;
		}

		public void Freeze()
		{
			_RB.constraints = RigidbodyConstraints2D.FreezePositionX;
			_SR.color = new Color(113.0f / 255.0f, 124.0f / 255.0f, 227.0f / 255.0f);
			_Anim.speed = 0.0f;
		}

		public void UnFreeze()
		{
			_RB.constraints = RigidbodyConstraints2D.None | RigidbodyConstraints2D.FreezeRotation;
			_SR.color = new Color(1.0f, 1.0f, 1.0f);
			_Anim.speed = 1.0f;
		}

    #endregion


}
