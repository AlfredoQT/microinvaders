﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class TenticleBehavior : MonoBehaviour
{

	#region GlobalVars

		[SerializeField, Tooltip("Minimum time at which the tenticle will rise")]
		private float _MinimumRiseTime = 1.5f;

		[SerializeField]
		private float _MaximumRiseTime = 2.2f;

		[SerializeField]
		private float _RelativeRiseHeight = 20.0f;

		[SerializeField]
		private float _StayOnRiseTime = 0.4f;

		[SerializeField]
		private float _DamageApplied = 20.0f;

		[SerializeField]
		private float _MinimumIncrementOnRise = 0.9f;

		[SerializeField]
		private float _MaximumIncrementOnRise = 1.3f;

		[SerializeField]
		private float _MinIncrementOnDown = 0.5f;

		[SerializeField]
		private float _MaxIncrementOnDown = 0.5f;

		[SerializeField]
		private bool _RandomizeX = false;

		[SerializeField, Tooltip("Only set this if randomize on X is turned on")]
		private float _RelativeMinX = -50.0f;

		[SerializeField, Tooltip("Only set this if randomize on X is turned on")]
		private float _RelativeMaxX = 50.0f;

		private Vector3 _InitialPositon;
		private float _TimeLeftToRise = 0.0f;
		private FiniteStateMachine _FSM;

	#endregion

	#region LifeCycle

		private void Awake()
		{
			_FSM = gameObject.AddComponent(typeof(FiniteStateMachine)) as FiniteStateMachine;
		}

		private void Start()
		{
			_TimeLeftToRise = UnityEngine.Random.Range(_MinimumRiseTime, _MaximumRiseTime);
			_InitialPositon = transform.localPosition;
			_RelativeRiseHeight += transform.localPosition.y;
			_FSM.SetState(Hidden());
		}

		private void OnCollisionStay2D(Collision2D col)
		{
			if (col.collider.CompareTag("Player"))
			{
				Health health = col.collider.GetComponent<Health>();
				if (health != null)
				{
					health.ReceiveDamage(_DamageApplied);
				}
			}
		}

	#endregion

	#region RisingStates

		private IEnumerator Hidden()
		{
			if (_RandomizeX)
			{
				transform.localPosition = new Vector3(_InitialPositon.x + UnityEngine.Random.Range(_RelativeMinX, _RelativeMaxX), 
										transform.localPosition.y, transform.localPosition.z);
			}
			while (_TimeLeftToRise > 0.0f)
			{
				_TimeLeftToRise -= Time.deltaTime;
				yield return null;
			}
			_TimeLeftToRise = UnityEngine.Random.Range(_MinimumRiseTime, _MaximumRiseTime);
			_FSM.SetState(HiddenToRise());
		}

		private IEnumerator HiddenToRise()
		{
			float increment = UnityEngine.Random.Range(_MinimumIncrementOnRise, _MaximumIncrementOnRise);
			while (transform.localPosition.y < _RelativeRiseHeight)
			{
				transform.localPosition += increment * Vector3.up;
				yield return null;
			}
			_FSM.SetState(Rise());
		}

		private IEnumerator Rise()
		{
			yield return new WaitForSeconds(_StayOnRiseTime);
			_FSM.SetState(RiseToHidden());
		}

		private IEnumerator RiseToHidden()
		{
			float increment = UnityEngine.Random.Range(_MinIncrementOnDown, _MaxIncrementOnDown);
			while (transform.localPosition.y  > _InitialPositon.y)
			{
				transform.localPosition -= increment * Vector3.up;
				yield return null;
			}
			_FSM.SetState(Hidden());
		}

	#endregion
}
