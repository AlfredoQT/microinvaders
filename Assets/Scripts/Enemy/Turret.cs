﻿using System;
using UnityEngine;

[RequireComponent(typeof(Health))]
[RequireComponent(typeof(Collider2D))]
public class Turret : MonoBehaviour
{

	#region GlobalVars

		private Health _Health;
		private Collider2D _Collider;
		private TurretMouth _Mouth;

	#endregion

	#region LifeCycle

		private void Awake()
		{
			_Health = GetComponent<Health>();
			_Collider = GetComponent<Collider2D>();
			_Mouth = GetComponentInChildren<TurretMouth>();
		}

		private void Update()
		{
			HandleDeath();
		}

		private void HandleDeath()
		{
			if (_Health.Dead)
			{
				Destroy(_Mouth.gameObject);
				_Collider.enabled = false;
				Destroy(this);
			}
			

		}

    #endregion

}
