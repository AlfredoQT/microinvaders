﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(SpriteRenderer))]
public class WormEnemy : MonoBehaviour, IFreeze
{
	
	#region GlobalVariables

		[Header("Player detection settings")]

		[SerializeField, Tooltip("The player")]
		private Transform _Target;

		[SerializeField]
		private float _DetectionRadius = 12.0f;

		[SerializeField]
		private float _LostSightRadius = 17.0f;

		[SerializeField]
		private LayerMask _DetectionLayer;

		[Space]

		[Header("Projectile Settings")]

		[SerializeField]
		private float _ShootDelay = 1.042f;

		[SerializeField]
		private HomingProjectile _ProjectilePrefab;

		[SerializeField]
		private float _ProjectileDamage = 20.0f;

		[SerializeField]
		private float _ProjectileMovementSpeed = 7.0f;
		
		private FiniteStateMachine _FSM;
		private Animator _Anim;
		private WormMouth _Mouth;
		private float _MouthAngle;
		private bool _Frozen = false;
		private SpriteRenderer _SR;
		
	#endregion

	#region UnityMethods

		private void Awake()
		{
			_FSM = gameObject.AddComponent(typeof(FiniteStateMachine)) as FiniteStateMachine;
			_Anim = GetComponent<Animator>();
			_Mouth = GetComponentInChildren<WormMouth>();
			_SR = GetComponent<SpriteRenderer>();
		}

		private void Start()
		{
			_MouthAngle = Mathf.Atan2(_Mouth.transform.right.y, _Mouth.transform.right.x);
			_FSM.SetState(Sleep());
		}
	
	#endregion

	#region StateMachine

		private bool TargetInRadius(float pRadius)
		{
			Collider2D collider = Physics2D.OverlapCircle(transform.position, pRadius, _DetectionLayer);
			if (collider != null)
			{
				return collider.CompareTag("Player");
			}
			return false;
		}

		private IEnumerator Sleep()
		{
		 	while (!TargetInRadius(_DetectionRadius))
		 	{
		 		yield return null;
		 	}
			_Anim.SetBool("IsSleeping", false);
		 	_FSM.SetState(Spawning());
		}

		private IEnumerator Spawning()
		{
			AudioManager.Instance.PlaySound("WormScream");
			yield return null;
			_FSM.SetState(Idle());
		}

		private IEnumerator Idle()
		{
			while(!TargetInRadius(_LostSightRadius))
			{
				yield return null;
			}
			_Anim.SetBool("IsAttacking", true);
			_FSM.SetState(Attack());
		}

		private IEnumerator Attack()
		{
			float shootTimer = 0f;
			while(TargetInRadius(_LostSightRadius))
			{
				shootTimer += Time.deltaTime;
				if (shootTimer >= _ShootDelay && !_Frozen)
				{
					shootTimer = 0f;
					AudioManager.Instance.PlaySound("WormSpit");
					HomingProjectile p = Instantiate(_ProjectilePrefab, _Mouth.transform.position, Quaternion.identity);
					p.Damage = _ProjectileDamage;
					p.Shoot(_Target, _ProjectileMovementSpeed);
				}
				yield return null;
			}
			_Anim.SetBool("IsAttacking", false);
			_FSM.SetState(Idle());
		}

		private void OnDrawGizmos()
		{
			Gizmos.DrawWireSphere(transform.position, _DetectionRadius);
			Gizmos.DrawWireSphere(transform.position, _LostSightRadius);
		}

		public void Freeze()
		{
			_SR.color = new Color(122.0f / 255.0f, 222.0f / 255.0f, 1.0f);
			_Anim.speed = 0.0f;
			_Frozen = true;
		}

		public void UnFreeze()
		{
			_SR.color = new Color(1.0f, 1.0f, 1.0f);
			_Anim.speed = 1.0f;
			_Frozen = false;
		}

    #endregion

}
