﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Boing : MonoBehaviour
{

    private Collider2D _Collider;

    private void Awake()
    {
        _Collider = GetComponent<Collider2D>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        AudioManager.Instance.PlaySound("AcidBurn");
    }
}