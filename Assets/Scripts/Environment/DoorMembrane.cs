﻿using UnityEngine;

[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(AudioSource))]
public class DoorMembrane : MonoBehaviour
{

	#region GlobalVariables

		private Collider2D _Collider;
		private Animator _Anim;
		private AudioSource _AS;

	#endregion

	#region LifeCycle

		private void Awake()
		{
			_Collider = GetComponent<Collider2D>();
			_Anim = GetComponent<Animator>();
			_AS = GetComponent<AudioSource>();
		}

	#endregion

	#region OpenDoor

		// To be activated by some event
		public void Open()
		{
			// Disable collider
			_Collider.enabled = false;
			_Anim.SetTrigger("Opened");
			_AS.Play();
		}

	#endregion

}
