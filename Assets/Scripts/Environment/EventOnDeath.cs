﻿using UnityEngine;
using UnityEngine.Events;

// Event to be called when a bunch of children with health components die
public class EventOnDeath : MonoBehaviour
{
	
	#region GlobalVars

		[SerializeField]
		private UnityEvent _OnDeath;

		private Health[] _HealthArray;

	#endregion

	#region LifeCycle

		private void Awake()
		{
			_HealthArray = GetComponentsInChildren<Health>();
		}

		private void Update()
		{
			if (AreDead())
			{
				_OnDeath.Invoke();
				Destroy(gameObject);
			}
		}

	#endregion

	#region ClassMethods

		private bool AreDead()
		{
			foreach (Health health in _HealthArray)
			{
				if (health != null)
				{
					return false;
				}
			}
			return true;
		}

	#endregion

}
