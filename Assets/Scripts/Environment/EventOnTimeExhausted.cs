﻿using UnityEngine;
using UnityEngine.Events;

public class EventOnTimeExhausted : MonoBehaviour
{

	#region GlobalVars

		[SerializeField]
		private UnityEvent _EventOnTimeExhausted;

		[SerializeField]
		private Timer _Timer;

	#endregion

	#region EventHandling
		
		private void Update()
		{
			if (_Timer.TimeLeft <= 0.0f)
			{
				// This timer is going to be reused
				_EventOnTimeExhausted.Invoke();
				_Timer.StopTimer();
			}
		}

	#endregion

}
