﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{

	#region GlobalVars

		[SerializeField]
		private float _TimerSeconds = 30.0f;

		internal float TimeLeft;
		private bool _Started = false;

	#endregion

	#region LifeCycle

		private void Start()
		{
			TimeLeft = _TimerSeconds;
		}

		private void Update()
		{
			if (_Started)
			{
				TimeLeft = TimeLeft <= 0.0f ? 0.0f : TimeLeft - Time.deltaTime;
			}
		}

	#endregion

	#region TimerMethods

		public void StartTimer()
		{
			_Started = true;
		}

		public void StopTimer()
		{
			TimeLeft = _TimerSeconds;
			_Started = false;
		}

	#endregion

}
