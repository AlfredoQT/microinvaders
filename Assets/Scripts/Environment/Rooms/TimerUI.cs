﻿using UnityEngine;
using UnityEngine.UI;

public class TimerUI : MonoBehaviour
{

	#region GlobalVars

		[SerializeField]
		private Text _TimerText;

		[SerializeField]
		private Timer _Timer;

	#endregion

	#region UI

		private void Update()
		{
			if (_TimerText.IsActive())
			{
				_TimerText.text = "Time Left: " + _Timer.TimeLeft.ToString("0.0");
			}
		}

	#endregion

}
