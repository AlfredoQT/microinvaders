﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

	// Change all of this later
	[SerializeField]
	internal int HealthUpgradeCost;

	[SerializeField]
	internal int WideBeamUpgradeCost;

	[SerializeField]
	internal int FreezeBeamUpgradeCost;

	private Vector3 _LastCheckpoint;

	internal RayGun CurrentRayGun = new RayGun();

	internal int GemCount = 0;

	internal int Lives = 2;

	public static GameManager Instance;

	// I will change this in the final release
	internal bool HealthUpgradePurchased = false;
	internal bool WideBeamPurchased = false;
	internal bool FreezeBeamPurchased = false;
	internal bool CheckpointSet = false;

	public Vector3 LastCheckpoint
	{
		set
		{
			_LastCheckpoint = value;
			CheckpointSet = true;
		}
		get
		{
			return _LastCheckpoint;
		}
	}

	private void Awake()
	{
		if (Instance ==  null)
		{
			Instance = this;
			
		}
		else
		{
			Destroy(gameObject);
			return;
		}
		// Persistent
		DontDestroyOnLoad(gameObject);
	}

    private void Start()
    {
        EventManager.StartListening("PlayerDies", HandlePlayerDeath);
		EventManager.StartListening("SceneAboutToLoad", ResetCheckpoints);
    }

    private void HandlePlayerDeath()
    {
        if (Lives > 0)
		{
			--Lives;
			GemCount = 0;
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
		}
		else
		{
			Lives = 3;
			CurrentRayGun = new RayGun();
			GemCount = 0;
			HealthUpgradePurchased = false;
			WideBeamPurchased = false;
			FreezeBeamPurchased = false;
			CheckpointSet = false;
			SceneManager.LoadScene("GameOver");
		}
    }

	private void ResetCheckpoints()
	{
		CheckpointSet = false;
	}
}
