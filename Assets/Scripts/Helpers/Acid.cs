﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Collider2D))]
public class Acid : MonoBehaviour
{

    #region GlobalVariables

    [SerializeField]
    private float _Damage = 5.0f;

    [SerializeField]
    private float _GravityScale = 1.0f;

    private Rigidbody2D _RB;
    private Collider2D _Collider;

    #endregion

    #region LifeCycle

    private void Awake()
    {
        _RB = GetComponent<Rigidbody2D>();
        _Collider = GetComponent<Collider2D>();
    }

    private void Start()
    {
        _RB.gravityScale = _GravityScale;
        _Collider.isTrigger = true;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Health health = other.GetComponent<Health>();
        if (health != null)
        {
            health.ReceiveDamage(_Damage);

            AudioManager.Instance.PlaySound("AcidBurn");

        }
    }

    #endregion


}
