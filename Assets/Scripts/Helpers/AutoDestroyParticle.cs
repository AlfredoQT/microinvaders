﻿using UnityEngine;

[RequireComponent(typeof(AutoDestroyParticle))]
public class AutoDestroyParticle : MonoBehaviour
{

	private ParticleSystem _PS;

	private void Awake()
	{
		_PS = GetComponent<ParticleSystem>();
	}

	private void Update()
	{
		if (!_PS.isPlaying)
		{
			Destroy(gameObject);
		}
	}

}
