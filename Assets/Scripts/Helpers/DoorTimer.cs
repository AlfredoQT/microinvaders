﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(BoxCollider2D))]
public class DoorTimer : MonoBehaviour 
{
	[SerializeField]
	private GameObject _Door;
	void Start () 
	{
		
	}

	void Update ()
	{
		 StartCoroutine(OPenDoor());
	}
	
	 IEnumerator OPenDoor() 
   	{
         yield return new WaitForSeconds(5.0f);
         Destroy(_Door);
    }

	/*
	void OnTriggerEnter2D(Collider2D collider) 
	{ 
          if(collider.tag=="Player")
		  {
          PlaySound(0);
          Scores.AddPoint();
          GetComponent<BoxCollider2D> ().enabled = false;
          StartCoroutine(OPenDoor());
      }
 
   IEnumerator OPenDoor() 
   {
         yield return new WaitForSeconds(5.0f);
         GetComponent<BoxCollider2D> ().enabled = true;
     }
	 */
	
}
