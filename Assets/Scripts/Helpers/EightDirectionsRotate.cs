﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EightDirectionsRotate : IRotate
{

	// The eight angles
	private float[] _Angles;

	public EightDirectionsRotate()
	{
		_Angles = new float[] {0.0f, 45.0f, 90.0f, 135.0f, 180.0f, 225.0f, 270.0f, 315.0f};
	}

	// This guy receives the angle in degrees
    public void RotateTo(Transform transform, float angle)
    {
		// Find the difference with every angle
		float minDiff = Mathf.Abs(angle - _Angles[0]);
		int minIndex = 0;
		for (int i = 1; i < _Angles.Length; ++i)
		{
			float currentDiff = Mathf.Abs(angle - _Angles[i]);
			if (currentDiff < minDiff)
			{
				minDiff = currentDiff;
				minIndex = i;
			}
		}

        // Rotate it to the one with the least difference around the z-axis
		Vector3 newRotation = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, _Angles[minIndex]);

		transform.eulerAngles = newRotation;
    }

}
