﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// A wrapper for simple state machines
public class FiniteStateMachine : MonoBehaviour
{

	private IEnumerator _CurrentState = null;

	public void SetState(IEnumerator newState)
	{
		if (_CurrentState != null)
			{
				StopCoroutine(_CurrentState);
			}
			_CurrentState = newState;
			StartCoroutine(_CurrentState);
	}

}
