﻿using System.Linq;
using UnityEngine;

public class Freezer : MonoBehaviour
{
	#region GlobalVariables

		internal bool Frozen;

		private IFreeze[] _Freezes;

	#endregion

	#region LifeCycle

		private void Awake()
		{
			// _RB = GetComponent<Rigidbody2D>();

			MonoBehaviour[] monos = GetComponents<MonoBehaviour>();

			// Get all the components that implement IFreeze interface
			_Freezes = (from a in monos where a.GetType().GetInterfaces().Any(k => k == typeof(IFreeze)) select (IFreeze)a).ToArray();
		}

		// private void OnDestroy()
		// {
		// 	_RB.constraints = RigidbodyConstraints2D.None | RigidbodyConstraints2D.FreezeRotation;
		// }

	#endregion

	#region Freezing

		public void Freeze(float duration)
		{
			// _RB.constraints = RigidbodyConstraints2D.FreezePositionX;

			foreach (var item in _Freezes)
			{
				item.Freeze();
			}

			Frozen = true;
			Invoke("UnFreeze", duration);
		}

		private void UnFreeze()
		{
			// _RB.constraints = RigidbodyConstraints2D.None | RigidbodyConstraints2D.FreezeRotation;

			foreach (var item in _Freezes)
			{
				item.UnFreeze();
			}

			Frozen = false;
		}

	#endregion
}
