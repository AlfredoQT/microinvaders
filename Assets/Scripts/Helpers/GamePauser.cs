﻿using UnityEngine;

public class GamePauser : MonoBehaviour
{

	#region Pause/Unpause

		private void OnEnable()
		{
			Time.timeScale = 0.0f;
		}

		private void OnDisable()
		{
			Time.timeScale = 1.0f;
		}

	#endregion

}
