﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grounded : MonoBehaviour
{

	[Header("Grounded Settings")]

	[Space]

	[Tooltip("Offset")]
	[SerializeField]
	internal float XOffset = 0.1f;

	[SerializeField]
	internal float YOffset = 0.0f;

	[Tooltip("Minimum distance from Y to the ground to be considered grounded. Pivot should be in Y bottom")]
	[SerializeField]
	internal float RaycastDistance = 0.05f;

	[SerializeField]
	private LayerMask _LayerFilter;

	internal bool isGrounded = false;
	internal bool isPrevGrounded = false;
	internal float InitialYOffset;
	internal float InitialXOffset;
	internal float InitialRaycastDistance;

	private void Start()
	{
		InitialXOffset = XOffset;
		InitialYOffset = YOffset;
		InitialRaycastDistance = RaycastDistance;
	}

	private void Update()
	{
		isGrounded = IsGrounded(-XOffset) | IsGrounded(XOffset);
	}

	private void LateUpdate()
	{
		isPrevGrounded = isGrounded;
	}

	public bool IsGrounded(float xOffset)
	{
		Vector2 origin = transform.position;
		origin.x += xOffset;
		origin.y += YOffset;

		// Do the raycast to the ground
		RaycastHit2D hit = Physics2D.Raycast(origin, Vector2.down, RaycastDistance, _LayerFilter);

		Debug.DrawRay(origin, Vector3.down * RaycastDistance);

		return hit;
	}
}
