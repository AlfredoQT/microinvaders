﻿using System;
using UnityEngine;

public class Health : MonoBehaviour
{

	#region Serializables

		[SerializeField]
		internal float MaxHealth = 100f;

		[SerializeField]
		internal bool DestroyOnDeath = false;

		[SerializeField]
		private float _TimeToDestroy = 0.0f;

		[SerializeField]
		protected string _HurtAudioClip;

		[SerializeField]
		private string _DeathAudioClip;

		[SerializeField]
		private AnimationClip _DeathAnimClip;

		[SerializeField]
		private GameObject _DeathParticles;

		[SerializeField]
		private Vector3 _DeathParticleOffset;

	#endregion

	#region Members

		internal float CurrentHealth;
		internal bool Dead = false;

		protected Animator _Anim;
		protected AudioSource _AS;

	#endregion

	#region LifeCycle

		private void Awake()
		{
			_Anim = GetComponent<Animator>();
		}

		protected void Start()
		{
			CurrentHealth = MaxHealth;	
		}

	#endregion

	#region ClassMethods

		public virtual void ReceiveDamage(float amount)
		{
			// Do nothing if I'm dead
			if (Dead)
			{
				return;
			}

			// Take the damage
			CurrentHealth -= amount;

			if (String.Compare(_HurtAudioClip, "") != 0)
			{
				AudioManager.Instance.PlaySound(_HurtAudioClip);
			}

			// Kill me
			if (CurrentHealth <= 0.0f)
			{
				Die();
			}
		}

		public virtual void ReceivePoints(float points)
		{
			if (CurrentHealth >= MaxHealth)
			{
				return;
			}

			CurrentHealth += points;
		}

		public virtual void Die()
		{
			Dead = true;
			if (_DeathParticles != null)
			{
				Instantiate(_DeathParticles, transform.position + _DeathParticleOffset, Quaternion.identity);
			}
			if (String.Compare(_DeathAudioClip, "") != 0)
			{
				AudioManager.Instance.PlaySound(_DeathAudioClip);
			}

			if (DestroyOnDeath)
			{
				float deathAnimClipTime = 0.0f;

				if (_Anim != null)
				{
					if (_DeathAnimClip != null)
					{
						_Anim.SetTrigger("Die");
						deathAnimClipTime = _DeathAnimClip.length;
					}
				}

				Destroy(gameObject, deathAnimClipTime + _TimeToDestroy);
			}
		}

	#endregion

}
