﻿using System;
using UnityEngine;

public class PlayerHealth : Health
{

	#region GlobalVariables

		[SerializeField]
		private SpriteRenderer[] _SRToFlash;

		[SerializeField]
		private float _InvincibilityTime = 1.0f;

		[SerializeField]
		internal float MaxExtraHealth = 40.0f;

		internal float CurrentExtraHealth;
		private float _InvincibilityTimer = 0.0f;
		private Color[] _SRInitialColors;

	#endregion

	#region ClassMethods

		private void Start()
		{
			base.Start();
			_SRInitialColors = new Color[_SRToFlash.Length];

			for (int i = 0; i < _SRToFlash.Length; ++i)
			{
				_SRInitialColors[i] = _SRToFlash[i].color;
			}
			CurrentExtraHealth = MaxExtraHealth;

			EventManager.StartListening("HealthUpgradeApplied", ResetHealth);
		}

		private void ResetHealth()
		{
			CurrentHealth = MaxHealth;
		}

    	private void Update()
		{
			_InvincibilityTimer = _InvincibilityTimer > 0.0f ? _InvincibilityTimer - Time.deltaTime : 0.0f;
			DrawSelfFlash(new Color(1.0f, 1.0f, 1.0f, 0.2f), 12, _InvincibilityTimer);
		}

		public override void ReceiveDamage(float amount)
		{
			// Do nothing if I'm dead
			if (Dead)
			{
				return;
			}

			if (_InvincibilityTimer > 0.0f)
			{
				return;
			}

			_InvincibilityTimer = _InvincibilityTime;

			if (GameManager.Instance.HealthUpgradePurchased)
			{
				
				if (CurrentExtraHealth <= 0.0f)
				{
					CurrentHealth -= amount;
				}
				else
				{
					CurrentExtraHealth -= amount;
				}
			}
			else
			{
				// Take the damage
				CurrentHealth -= amount;
			}

			AudioManager.Instance.PlaySound("CharacterHurt");
			

			// Kill me
			if (CurrentHealth <= 0.0f)
			{
				Die();
			}
		}

		public override void ReceivePoints(float points)
		{
			if (GameManager.Instance.HealthUpgradePurchased)
			{
				if (CurrentExtraHealth >= MaxExtraHealth)
				{
					return;
				}
				if (CurrentHealth < MaxHealth)
				{
					float pointsForExtra = points - (MaxHealth - CurrentHealth);
					CurrentHealth = Mathf.Clamp(CurrentHealth + points, 0f, MaxHealth);
					CurrentExtraHealth = Mathf.Clamp(CurrentExtraHealth + pointsForExtra, 0f, MaxExtraHealth);
				}
				else
				{
					CurrentExtraHealth = Mathf.Clamp(CurrentExtraHealth + points, 0f, MaxExtraHealth);
				}
				return;
			}
			if (CurrentHealth >= MaxHealth)
			{
				return;
			}

			CurrentHealth = Mathf.Clamp(CurrentHealth + points, 0f, MaxHealth);
		}

		private void DrawSelfFlash(Color flashColor, float interval, float timer)
		{
			// Do nothing if the sprite renderer is not found
			if (_SRToFlash.Length == 0)
			{
				return;
			}

			if (timer > 0.0f)
			{
				// Just by chance for now, I cannot figure out a better way for now
				float chance = UnityEngine.Random.Range(0, 1.0f);
				for (int i = 0; i < _SRToFlash.Length; ++i)
				{
					_SRToFlash[i].color = chance < 0.5f ? flashColor : _SRInitialColors[i];
				}
			}
			else
			{
				for (int i = 0; i < _SRToFlash.Length; ++i)
				{
					_SRToFlash[i].color = _SRInitialColors[i];
				}
			}
		}

		public override void Die()
		{
			Dead = true;
			AudioManager.Instance.PlaySound("CharacterDeath");
			_Anim.SetBool("Death", true);
			_Anim.SetTrigger("Die");
			Time.timeScale = 0.2f;
			EventManager.TriggerEvent("PlayerFrozen");
			EventManager.TriggerEvent("PlayerShotToDead");
			Invoke("DoDeath", 0.6f);
		}

		private void DoDeath()
		{
			Time.timeScale = 1.0f;
			EventManager.TriggerEvent("PlayerDies");
		}

	#endregion
}
