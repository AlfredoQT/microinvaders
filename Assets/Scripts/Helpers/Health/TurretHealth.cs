﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretHealth : Health
{
	public override void Die()
	{
		Dead = true;
		_Anim.SetTrigger("Die");
	}
}
