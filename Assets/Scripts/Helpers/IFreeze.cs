﻿using UnityEngine;

public interface IFreeze
{

	void Freeze();
	void UnFreeze();

}
