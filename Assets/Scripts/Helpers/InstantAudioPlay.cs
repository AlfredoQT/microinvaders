﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class InstantAudioPlay : MonoBehaviour
{	

	private AudioSource _AS;
	private float _AudioClipLength;

	private void Awake()
	{
		_AS = GetComponent<AudioSource>();
	}

	private void Start()
	{
		// Try to get the audio clip
		_AudioClipLength = _AS.clip.length;
		_AS.Play();
		Destroy(gameObject, _AudioClipLength);
	}

}
