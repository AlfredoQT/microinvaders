﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRotate
{
	void RotateTo(Transform transform, float angle);
}
