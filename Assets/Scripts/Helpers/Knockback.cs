﻿using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Knockback : MonoBehaviour 
{

	[SerializeField]
	private float _KnockbackForce = 25.0f;

	private void OnCollisionStay2D(Collision2D collision)
	{
		if (collision.collider.CompareTag("Player"))
		{
			if (collision.contacts.Length > 0)
			{
				Vector2 knockbackDir = -collision.contacts[0].normal;
				Rigidbody2D otherRB = collision.collider.GetComponent<Rigidbody2D>();
				if (otherRB != null)
				{
					otherRB.velocity = knockbackDir * _KnockbackForce;
				}
			}
		}
	}
		
}








