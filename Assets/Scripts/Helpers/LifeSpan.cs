﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeSpan : MonoBehaviour
{

	[SerializeField]
	private float _LifeTime = 5.0f;

	private void Start()
	{
		Destroy(gameObject, _LifeTime);
	}

}
