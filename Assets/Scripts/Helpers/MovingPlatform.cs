﻿using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class MovingPlatform : MonoBehaviour
{

	#region MovementSettings 

		[SerializeField]
		private Vector3 _EndPosition;

		[SerializeField]
		private float _Duration = 3.0f;

		private float _Time = 0.0f;
		private Vector3 _StartPosition;

		private bool _Moving = false;

	#endregion

	#region Movement
		
		private void Start()
		{
			_StartPosition = transform.localPosition;
		}

		private void FixedUpdate()
		{
			if (_Moving)
			{
				_Time = _Time < 1.0f ? _Time + Time.fixedDeltaTime / _Duration : 1.0f;
			}
			transform.localPosition = Vector3.Lerp(_StartPosition, _EndPosition, _Time);
		}

		private void OnCollisionExit2D(Collision2D other)
		{
			if (other.collider.CompareTag("Player"))
			{
				ResetPosition();
			}
		}

		public void MoveToEnd()
		{
			_Time = 0.0f;
			_Moving = true;
		}

		public void ResetPosition()
		{
			transform.localPosition = _StartPosition;
			_Time = 0.0f;
			_Moving = false;
		}

	#endregion

}
