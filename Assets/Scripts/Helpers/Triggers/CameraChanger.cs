﻿using UnityEngine;

public class CameraChanger : MonoBehaviour
{

    #region GlobalVariables

		[SerializeField]
		private Vector3 _NewOffset;

		[SerializeField]
		private MinMaxClamp _NewClampParams;

		[Range(0.0f, 1.0f)]
		[SerializeField]
		private float _NewSmoothing = 0.05f;

		private CameraFollow _MainCamera;

    #endregion

    #region LifeCycle

    	private void Awake()
		{
			_MainCamera = Camera.main.GetComponent<CameraFollow>();
		}

	#endregion

	#region ClassMethods

		public void UpdateCamera()
		{
			_MainCamera.CameraOffset = _NewOffset;
			_MainCamera.ClampParameters = _NewClampParams;
			_MainCamera.Smoothing = _NewSmoothing;
		}

	#endregion

}
