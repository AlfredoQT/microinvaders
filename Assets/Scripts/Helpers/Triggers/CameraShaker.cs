﻿using System.Collections;
using UnityEngine;

public class CameraShaker : MonoBehaviour
{

	#region ShakeProperties

		[SerializeField]
		private float _Duration = 3.0f;

		[SerializeField]
		private float _Magnitude = 0.5f;

		private Camera _MainCamera;

	#endregion

	#region Shake

		private void Awake()
		{
			_MainCamera = Camera.main;
		}

		public void BeginShake()
		{
			StartCoroutine(Shake());
			StopCoroutine(Shake());
		}

		private IEnumerator Shake()
		{
			float elapsed = 0.0f;

			//Vector3 originalCamPos = Camera.main.transform.localPosition;

			while (elapsed < _Duration)
			{

				elapsed += Time.unscaledDeltaTime;

				float percentComplete = elapsed / _Duration;
				float damper = 1.0f - Mathf.Clamp(4.0f * percentComplete - 3.0f, 0.0f, 1.0f);

				// map value to [-1, 1]
				float x = UnityEngine.Random.value * 2.0f - 1.0f;
				float y = UnityEngine.Random.value * 2.0f - 1.0f;
				x *= _Magnitude * damper;
				y *= _Magnitude * 0.5f * damper;

				_MainCamera.transform.position += new Vector3(x, y, 0.0f);

				yield return null;
			}
		}

	#endregion
}
