﻿using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider2D))]
public class EventOnTrigger : MonoBehaviour
{

	#region GlobalVars

		

		[SerializeField]
		private bool _TriggerOnce = true;

		[SerializeField]
		private UnityEvent _EventOnTriggerEnter;

		private Collider2D _Collider;

		private bool _Triggered = false;

	#endregion

	#region LifeCycle

		private void Awake()
		{
			_Collider = GetComponent<Collider2D>();
		}

		private void Start()
		{
			_Collider.isTrigger = true;

			gameObject.layer = LayerMask.NameToLayer("TriggerVolume");
		}

	#endregion

	#region TriggerMethods

		void OnTriggerEnter2D(Collider2D other)
		{
			if (_Triggered && _TriggerOnce)
			{
				return;
			}

			if (other.CompareTag("Player"))
			{
				_EventOnTriggerEnter.Invoke();

				_Triggered = true;
			}
		
		}

	#endregion

}
