﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class ParalaxTest : MonoBehaviour
{

    private Rigidbody2D _RB;
    [SerializeField]
    private float _ParalaxSpeed = 1f;

    [SerializeField]
    private int _DirectionX = 1;

    [SerializeField]
    private int _DirectionY = 0;

    private void Awake()
    {
        _RB = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        _RB.velocity = new Vector2(_DirectionX, _DirectionY) * Time.deltaTime * _ParalaxSpeed;
    }
}
