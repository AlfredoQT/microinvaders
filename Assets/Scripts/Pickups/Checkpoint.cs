﻿using UnityEngine;

public class Checkpoint : IPickable
{
    public void Pick(Collider2D other)
    {
        GameManager.Instance.LastCheckpoint = other.transform.position;
    }
}
