﻿using UnityEngine;

public class CheckpointCollectable : Collectable
{
    protected override void CollectableStart()
    {
        _Pickable = new Checkpoint();
    }
}
