﻿using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public abstract class Collectable : MonoBehaviour
{

	protected IPickable _Pickable;
	private Collider2D _Collider;

	private void Awake()
	{
		_Collider = GetComponent<Collider2D>();
	}

	private void Start()
	{
		_Collider.isTrigger = true;
		CollectableStart();
	}

	protected abstract void CollectableStart();

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag("Player"))
		{
			_Pickable.Pick(other);
			Destroy(gameObject);
		}
	}

}
