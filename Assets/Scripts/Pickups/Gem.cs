﻿using UnityEngine;


public class Gem : IPickable
{
    public void Pick(Collider2D other)
    {
        ++GameManager.Instance.GemCount;
        AudioManager.Instance.PlaySound("GemCollected");
    }
}
