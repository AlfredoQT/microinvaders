﻿using UnityEngine;

public class GemCollectable : Collectable
{

	protected override void CollectableStart()
	{
		_Pickable = new Gem();
	}
}
