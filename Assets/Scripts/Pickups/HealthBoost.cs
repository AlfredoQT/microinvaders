﻿using UnityEngine;

public class HealthBoost : IPickable
{

    public void Pick(Collider2D other)
    {
		Health health = other.GetComponent<Health>();
        if (health != null)
		{
			health.ReceivePoints(100.0f);
		}
        AudioManager.Instance.PlaySound("HealthCollected");
    }
}
