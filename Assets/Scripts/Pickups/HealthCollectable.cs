﻿using UnityEngine;

public class HealthCollectable : Collectable
{

    protected override void CollectableStart()
    {
        _Pickable = new HealthBoost();
    }
    
}
