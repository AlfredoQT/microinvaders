﻿using UnityEngine;

public interface IPickable
{
	void Pick(Collider2D other);
}
