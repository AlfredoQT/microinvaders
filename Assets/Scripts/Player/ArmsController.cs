﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct GunSettings
{
	public Sprite sprite;
	public Vector3 muzzleOffset;
}

[RequireComponent(typeof(SpriteRenderer))]
public class ArmsController : MonoBehaviour
{

	#region GlobalVars

		[Header("Eight Directions Settings")]

		[SerializeField]		
		private Transform _GunPoint;

		[SerializeField]
		private GunSettings _Up;

		[SerializeField]
		private GunSettings _UpLeft;

		[SerializeField]
		private GunSettings _Left;

		[SerializeField]
		private GunSettings _DownLeft;

		[SerializeField]
		private GunSettings _Down;

		[Space]

		[Header("Crouching settings")]

		[SerializeField]
		private Vector3 _OffsetOnCrouch;

		private PlayerUserControls _PUC;
		private Grounded _Grounded;
		private SpriteRenderer _SR;

		private IRotate _Rotate;
		private float _AngleOnRest = 0.0f;
		private bool _FacingRight = true;
		private bool _PlayerKilled = false;

	#endregion

	#region UnityMethods

		private void Awake()
		{
			_PUC = GetComponentInParent<PlayerUserControls>();
			_Grounded = GetComponentInParent<Grounded>();
			_SR = GetComponent<SpriteRenderer>();
		}

		private void Start()
		{
			_Rotate = new EightDirectionsRotate();
			EventManager.StartListening("PlayerShotToDead", SetPlayerKilled);
		}

		private void Update()
		{
			_FacingRight = _PUC.hDir < 0.0f ? false : (_PUC.hDir > 0.0f ? true : _FacingRight);

			_SR.flipY = !_FacingRight;

			bool isCrouching = _PUC.crouch && Mathf.Abs(_PUC.hDir) < 0.0001f && _Grounded.isGrounded;
			_SR.enabled = !isCrouching && !_PlayerKilled;

			if (isCrouching)
			{
				_Rotate.RotateTo(transform, _AngleOnRest);
				_GunPoint.localPosition = _OffsetOnCrouch;
				_GunPoint.localPosition = new Vector3(_GunPoint.localPosition.x, _FacingRight ? _GunPoint.localPosition.y : -_GunPoint.localPosition.y, _GunPoint.localPosition.z);
			}
			else
			{
				Rotate();
			}
			
		}

		private void SetPlayerKilled()
		{
			_PlayerKilled = true;
		}

    #endregion

	#region ClassMethods

		private void Rotate()
		{
			_AngleOnRest = _PUC.hDir < 0.0f ? 180.0f : (_PUC.hDir > 0.0f ? 0.0f : _AngleOnRest);

			bool isMoving = Mathf.Abs(_PUC.hDir) > 0.0001f || Mathf.Abs(_PUC.vDir) > 0.0001f;

			float angle = isMoving ? AngleToRotate() : _AngleOnRest;

			SetSprite(angle);

			_Rotate.RotateTo(transform, angle);
		}

		private void SetSprite(float angle, float epsilon = 0.005f)
		{
			if (Mathf.Abs(angle - 90.0f) < epsilon)
			{
				_SR.sprite = _Up.sprite;
				_GunPoint.localPosition = _Up.muzzleOffset;
			}
			else if (Mathf.Abs(angle - 45.0f) < epsilon || Mathf.Abs(angle - 135.0f) < epsilon)
			{
				_SR.sprite = _UpLeft.sprite;
				_GunPoint.localPosition = _UpLeft.muzzleOffset;
			}
			else if (Mathf.Abs(angle) < epsilon || Mathf.Abs(angle - 180.0f) < epsilon)
			{
				_SR.sprite = _Left.sprite;
				_GunPoint.localPosition = _Left.muzzleOffset;
			}
			else if (Mathf.Abs(angle - 225.0f) < epsilon || Mathf.Abs(angle - 315.0f) < epsilon)
			{
				_SR.sprite = _DownLeft.sprite;
				_GunPoint.localPosition = _DownLeft.muzzleOffset;

			}
			else if (Mathf.Abs(angle - 270.0f) < epsilon)
			{
				_SR.sprite = _Down.sprite;
				_GunPoint.localPosition = _Down.muzzleOffset;
			}
			_GunPoint.localPosition = new Vector3(_GunPoint.localPosition.x, _FacingRight ? -_GunPoint.localPosition.y : _GunPoint.localPosition.y, _GunPoint.localPosition.z);
		}

    	private float AngleToRotate()
		{
			float angle = Mathf.Atan2(_PUC.vDir, _PUC.hDir) * Mathf.Rad2Deg;
			return angle < 0.0f ? angle + 360.0f : angle;
		}

	#endregion

}
