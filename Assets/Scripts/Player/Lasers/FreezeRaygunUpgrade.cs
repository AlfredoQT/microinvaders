﻿using UnityEngine;

public class FreezeRaygunUpgrade : RayGunUpgrade
{
	
	public FreezeRaygunUpgrade()
	{
        Damage = 2.0f;
		Width = 0.15f;
		Type = RayGunUpgradeType.FREEZE;
		UpgradeStartSound = "CharacterStartedFiring";
		UpgradeSound = "CharacterFiringIce";
		UpgradeColor = new Color(0.0f / 255.0f, 255.0f / 255.0f, 234.0f / 255.0f, 128.0f / 255.0f);

	}

    public override void Special(Transform other)
    {
        Freezer freezer = other.GetComponent<Freezer>();
        if(freezer)
        {
            freezer.Freeze(1.5f);
        }

    }
}
