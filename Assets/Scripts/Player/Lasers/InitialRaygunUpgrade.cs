﻿using UnityEngine;

public class InitialRaygunUpgrade : RayGunUpgrade
{
	public InitialRaygunUpgrade()
	{
		Damage = 6.0f;
		Width = 0.05f;
		Type = RayGunUpgradeType.None;
		UpgradeStartSound = "CharacterStartedFiring";
		UpgradeSound = "CharacterFiring";
		UpgradeColor = new Color(28.0f / 255.0f, 225.0f / 255.0f, 70.0f / 255.0f, 128.0f / 255.0f);

	}

    public override void Special(Transform other) {}
}
