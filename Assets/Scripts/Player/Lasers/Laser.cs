﻿using UnityEngine;

// In MVC, this would be the V
[RequireComponent(typeof(LineRenderer))]
public class Laser : MonoBehaviour
{

    #region GlobalVars

        [SerializeField]
        private GameObject[] _EndParticles;

        private LineRenderer _LR;

        // In MVC, this would be the M
        internal RayGun RayInfo;
        private ParticleSystem[] _EndParticlesSystem;

    #endregion

    #region LifeCycle

        private void Awake()
        {
            _LR = GetComponent<LineRenderer>();
        }

        private void Start()
        {
            _LR.startWidth = RayInfo.Width;
            _LR.endWidth = RayInfo.Width;
            _LR.material.SetColor("_TintColor", RayInfo.RayColor);
            _EndParticlesSystem = new ParticleSystem[_EndParticles.Length];
            for (int i = 0; i < _EndParticles.Length; ++i)
            {
                _EndParticlesSystem[i] = Instantiate(_EndParticles[i]).GetComponent<ParticleSystem>();
            }
        }

        private void OnDestroy()
        {
            for (int i = 0; i < _EndParticles.Length; ++i)
            {
                if (_EndParticlesSystem != null)
                {
                    if (_EndParticlesSystem[i] != null)
                    {
                        Destroy(_EndParticlesSystem[i].gameObject);
                    }
                }
            }
        }

    #endregion

    #region View

        public void Generate(Vector3 startPos, Vector3 endPos)
        {
            _LR.SetPosition(0, startPos);
            RandomizePositions(startPos, endPos);
            _LR.SetPosition(_LR.positionCount - 1, endPos);
            for (int i = 0; i < _EndParticles.Length; ++i)
            {
                if (_EndParticlesSystem != null)
                {
                    if (_EndParticlesSystem[i] != null)
                    {
                        _EndParticlesSystem[i].transform.position = endPos;
                    }
                }
            }
        }

        private void RandomizePositions(Vector3 startPos, Vector3 endPos)
        {
            for (int i = 1; i < _LR.positionCount - 1; ++i)
            {
                Vector3 newPos = ((float)(_LR.positionCount - 1 - i) / (float)(_LR.positionCount - 1)) * startPos + ((float)i / (float)(_LR.positionCount - 1)) * endPos;
                newPos.x += Random.Range(-0.5f, 0.5f);
                newPos.y += Random.Range(-0.5f, 0.5f);
                _LR.SetPosition(i, newPos);
            }
        }

    #endregion

}
