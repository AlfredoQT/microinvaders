﻿using System.Collections;
using UnityEngine;

public class RayGun
{
	public float Damage
	{
		get
		{
			float max = -1f;
			// Find the one with the greates width
			foreach (var item in _Upgrades)
			{
				RayGunUpgrade upgrade = (RayGunUpgrade)item;
				if (upgrade.Damage > max)
				{
					max = upgrade.Damage;
				}
			}
			return max;
		}
	}
	private ArrayList _Upgrades;

	public string StartSound
	{
		get
		{
			// Just get the latest
			return ((RayGunUpgrade)_Upgrades[_Upgrades.Count - 1]).UpgradeStartSound;
		}
	}

	public string ShootSound
	{
		get
		{
			// Just get the latest
			return ((RayGunUpgrade)_Upgrades[_Upgrades.Count - 1]).UpgradeSound;
		}
	}
	
	public RayGunUpgradeType UpgradeFlags
	{
		get
		{
			RayGunUpgradeType flags = RayGunUpgradeType.None;
			foreach (var item in _Upgrades)
			{
				flags |= ((RayGunUpgrade)item).Type;
			}
			return flags;
		}
	}

	public float Width
	{
		get
		{
			float max = -1f;
			// Find the one with the greates width
			foreach (var item in _Upgrades)
			{
				RayGunUpgrade upgrade = (RayGunUpgrade)item;
				if (upgrade.Width > max)
				{
					max = upgrade.Width;
				}
			}
			return max;
		}
	}

	public Color RayColor
	{
		get
		{
			// Just get the latest
			return ((RayGunUpgrade)_Upgrades[_Upgrades.Count - 1]).UpgradeColor;
		}
	}

	public RayGun()
	{
		_Upgrades = new ArrayList();
		_Upgrades.Add(new InitialRaygunUpgrade());
	}


	public virtual void Attack(Transform other)
	{
		// Normal attack
		Health health = other.GetComponent<Health>();
		if (health != null)
		{
			health.ReceiveDamage(Damage);
		}
		// Apply the special attack of each upgrade
		foreach (var upgrade in _Upgrades)
		{
			RayGunUpgrade special = (RayGunUpgrade) upgrade;
			special.Special(other);
		}
	}

	public void AddUpgrade(RayGunUpgrade newUpgrade)
	{
		_Upgrades.Add(newUpgrade);
	}

}
