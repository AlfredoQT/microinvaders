﻿using UnityEngine;

[System.Flags]
public enum RayGunUpgradeType
{
	None = 0,
	WIDE = 1,
	FREEZE = 2
}

public abstract class RayGunUpgrade
{

	public float Damage;
	public float Width;
	public RayGunUpgradeType Type;
	public string UpgradeStartSound;
	public string UpgradeSound;
	public Color UpgradeColor;

	public abstract void Special(Transform other);

}
