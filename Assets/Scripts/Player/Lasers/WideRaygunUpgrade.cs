﻿using UnityEngine;

public class WideRaygunUpgrade : RayGunUpgrade
{

	public WideRaygunUpgrade()
	{
		Damage = 12.0f;
		Width = 0.5f;
		Type = RayGunUpgradeType.WIDE;
		UpgradeColor = new Color(49.0f / 255.0f, 153.0f / 255.0f, 255.0f / 255.0f, 128.0f / 255.0f);
		UpgradeStartSound = "CharacterStartedFiring";
		UpgradeSound = "CharacterFiringWide";
	}

    public override void Special(Transform other) {}
}
