using System;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(CapsuleCollider2D))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(PlayerUserControls))]
[RequireComponent(typeof(Grounded))]
[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Health))]
public class PlayerController : MonoBehaviour
{
	#region Serializables

		[Header("Movement Settings")]
		
		[SerializeField]
		private float _MovementSpeed = 6f;

		[Space]

		[Header("Jump Settings")]

		[SerializeField]
		private float _JumpSpeed = 12f;

		[Tooltip("Scale to be applied when falling from jump")]
		[SerializeField]
		private float _ScaleOnFall = 8f;

		[Tooltip("Scale to be applied when jump button is released")]
		[SerializeField]
		private float _ScaleOnRelease = 20f;

		[SerializeField, Tooltip("Don't worry about this Rodrigo")]
		private RefVector3 _PlayerPos;

		[SerializeField]
		private GameObject _JetpackParticles;
	
	#endregion

	#region Members

		private Rigidbody2D _RB;
		private Animator _Anim;
		private SpriteRenderer _SR;
		private Grounded _Grounded;
		private PlayerUserControls _PUC;
		private CapsuleCollider2D _Collider;

		private float _HDir = 0.0f;
		private bool _JumpRequested = false;
		private bool _Crouch = false;
		private bool _MovementStopped = false;
		private bool _ShouldStop = false;
		private bool _SoundJetPlayed = false;

	#endregion

	#region LifeCycle

		private void Awake()
		{
			_RB = GetComponent<Rigidbody2D>();
			_Anim = GetComponent<Animator>();
			_SR = GetComponent<SpriteRenderer>();
			_Grounded = GetComponent<Grounded>();
			_PUC = GetComponent<PlayerUserControls>();
			_Collider = GetComponent<CapsuleCollider2D>();
		}

		private void Start()
		{
			EventManager.StartListening("ResumedGame", EnableMovement);
			EventManager.StartListening("PausedGame", StopMovement);
			EventManager.StartListening("PlayerFrozen", StopAll);

			if (GameManager.Instance.CheckpointSet)
			{
				transform.position = GameManager.Instance.LastCheckpoint;
			}

			// Store the checkpoint
			GameManager.Instance.LastCheckpoint = transform.position;
		}

		private void StopAll()
		{
			_ShouldStop = true;
			StopMovement();
		}

		private void Update()
		{
			if (_ShouldStop)
			{
				return;
			}
			// Let everyone know about my position
			_PlayerPos.Data = transform.position;

			// Movement
			_HDir = _PUC.hDir;

			// Flip the sprite renderer
			_SR.flipX = _PUC.hDir < 0.0f ? true : (_PUC.hDir > 0.0f ? false : _SR.flipX);

			// Jump
			_JumpRequested |= _PUC.jump && _Grounded.isGrounded;

			// Crouching
			_Crouch = Mathf.Abs(_PUC.hDir) < 0.0001f && _PUC.crouch && _Grounded.isGrounded;

			SetColliderValues();

			// Animation
			Animate();

			// Sounds
			PlaySounds();

			// Particles
			PlayParticles();

		}

		private void PlayParticles()
		{
			if (_PUC.jumpHeld && !_Grounded.isGrounded && _RB.velocity.y > 0.0f)
			{
				_JetpackParticles.SetActive(true);
				if (!_SoundJetPlayed)
				{
					AudioManager.Instance.PlaySound("CharacterJet");
					_SoundJetPlayed = true;
				}
				
			}
			else
			{
				_JetpackParticles.SetActive(false);
				AudioManager.Instance.StopSound("CharacterJet");
				_SoundJetPlayed = false;
			}
		}

		private void FixedUpdate()
		{
			ApplyGravityScale();

			// Movement
			_RB.velocity = _MovementStopped ? Vector2.zero : new Vector2(_HDir * _MovementSpeed, _RB.velocity.y);

			// Jumping
			if (_JumpRequested)
			{
				_RB.velocity = new Vector2(_RB.velocity.x, _JumpSpeed);
				_JumpRequested = false;
			}
		}

	#endregion

	#region ClassMethods

		private void PlaySounds()
		{
			if (_PUC.jump && _Grounded.isGrounded)
			{
				AudioManager.Instance.PlaySound("CharacterJump");
			}
			if (_Grounded.isGrounded && !_Grounded.isPrevGrounded)
			{
				AudioManager.Instance.PlaySound("CharacterLanded");
			}
		}

		private void SetColliderValues()
		{
			if (_Crouch)
			{
				_Collider.offset = new Vector2(0.0f, 1.5f);
				_Collider.size = new Vector2(12.0f, 3.0f);
				_Collider.direction = CapsuleDirection2D.Horizontal;
			}
			else
			{
				_Collider.offset = new Vector2(0.0f, 5.0f);
				_Collider.size = new Vector2(3.1f, 10.0f);
				_Collider.direction = CapsuleDirection2D.Vertical;
			}
		}

		// Applies a gravity scale based on the velocity of the rigid body
		private void ApplyGravityScale()
		{
			// Falling
			if (_RB.velocity.y < 0.0f)
			{
				_RB.gravityScale = _ScaleOnFall;
			}
			// Make it feel like a low jump when jump key is no longer being held down
			else if (_RB.velocity.y > 0.0f && (_PUC.jumpReleased))
			{
				_RB.gravityScale = _ScaleOnRelease;
			}
			// Back to normal
			else
			{
				_RB.gravityScale = 1.0f;
			}
		}

		private void Animate()
		{
			// Move
			_Anim.SetBool("IsMoving", _MovementStopped ? false : Mathf.Abs(_HDir) > 0.001f);

			// Jump
			_Anim.SetBool("IsGrounded", _Grounded.isGrounded);

			// Crouch
			_Anim.SetBool("IsCrouching", _Crouch);
		}

		public void EnableMovement()
		{
			_MovementStopped = false;
			_RB.constraints = RigidbodyConstraints2D.None | RigidbodyConstraints2D.FreezeRotation;
			_PUC.enabled = true;
		}

		public void StopMovement()
		{
			_MovementStopped = true;
			_RB.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
			_PUC.enabled = false;
		}

    #endregion

}
