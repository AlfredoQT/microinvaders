﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUserControls : MonoBehaviour
{

	#region Controls

		[Header("User Controls - Keyboard")]

		[SerializeField]
		internal KeyCode JumpControl = KeyCode.Z;

		[SerializeField]
		private string _HorizontalMovementAxis = "Horizontal-MI";

		[SerializeField]
		private string _VerticalMovementAxis = "Vertical-MI";

		[SerializeField]
		private KeyCode _ShootControl = KeyCode.X;

		[Space]
		
		[Header("User Controls - Gamepad")]

		[SerializeField]
		internal KeyCode JumpControlJoystick = KeyCode.JoystickButton1;

		internal bool crouch;

		internal bool jump;

		internal bool jumpHeld;

		internal float hDir;

		internal float vDir;

		internal bool shootDown;
		internal bool shoot;
		internal bool shootReleased;

		internal bool jumpReleased;

	#endregion

	#region UnityMethods

		private void Update ()
		{
			crouch = Mathf.Sign(Input.GetAxisRaw(_VerticalMovementAxis)) < 0.0f;
			jump = Input.GetKeyDown(JumpControl) || Input.GetButtonDown("Jump");
			jumpHeld = Input.GetKey(JumpControl) || Input.GetButton("Jump");
			hDir = Mathf.Abs(Input.GetAxisRaw(_HorizontalMovementAxis)) > 0.001f ? Mathf.Sign(Input.GetAxisRaw(_HorizontalMovementAxis)) : 0.0f;
			vDir = Mathf.Abs(Input.GetAxisRaw(_VerticalMovementAxis)) > 0.001f ? Mathf.Sign(Input.GetAxisRaw(_VerticalMovementAxis)) : 0.0f;
			shootDown = Input.GetKeyDown(_ShootControl) || Input.GetButtonDown("Fire1");
			shoot = Input.GetKey(_ShootControl) || Input.GetButton("Fire1");
			shootReleased = Input.GetKeyUp(_ShootControl) || Input.GetButtonUp("Fire1");
			jumpReleased = !Input.GetKey(JumpControl) && !Input.GetButton("Jump");
		}

	#endregion
}
