﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// In MVC, this would be the C
[RequireComponent(typeof(PlayerUserControls))]
public class PlayerWeapon : MonoBehaviour
{
	#region GlobalVariables

		[Header("Shooting Settings")]

		[SerializeField]
		private Transform _GunPoint;

		[SerializeField, Tooltip("This is just the view")]
		internal Laser _Laser;

		[SerializeField]
		private LayerMask _LayerFilter;

		[SerializeField]
		private float _ShootingDistance = 60.0f;

		[Tooltip("This will randomize the shooting")]
		[SerializeField]
		private float _AngleToRandomize = 5.0f;

		[SerializeField]
		private GameObject _MuzzleParticle;

		private PlayerUserControls _PUC;
		internal RayGun _RayGun;
		private Laser _LaserHolder;
		private Camera _CameraMain;
		private bool _Deactivate = false;
		private GameObject _MuzzleParticleHolder;

	#endregion

	#region LifeCycle

		private void Awake()
		{
			_PUC = GetComponent<PlayerUserControls>();
		}

		private void Start()
		{
			SetWeapon();
			_CameraMain = Camera.main;
			EventManager.StartListening("PlayerFrozen", Deactivate);
		}

		private void Update()
		{
			if (_Deactivate)
			{
				return;
			}
			ShootLaser();
			PlaySounds();

			if (_MuzzleParticleHolder != null)
			{
				_MuzzleParticleHolder.transform.position = _GunPoint.position;
			}
		}

		private void Deactivate()
		{
			AudioManager.Instance.StopSound(_RayGun.StartSound);
			AudioManager.Instance.StopSound(_RayGun.ShootSound);
			if (_LaserHolder !=  null)
			{
				Destroy(_LaserHolder.gameObject);
				_LaserHolder = null;
			}
			_Deactivate = true;
		}

	#endregion

	#region Shooting

		private void ShootLaser()
		{
			if (_PUC.shoot)
			{
				if (_LaserHolder == null)
				{
					_LaserHolder = Instantiate(_Laser);
				 	_LaserHolder.RayInfo = _RayGun;
				}

                // Slightly randomize the direction
                Vector2 dir = _GunPoint.right.normalized;
                float angle = Mathf.Atan2(dir.y, dir.x) + UnityEngine.Random.Range(-_AngleToRandomize * Mathf.Deg2Rad, _AngleToRandomize * Mathf.Deg2Rad);
                dir.x = Mathf.Cos(angle);
                dir.y = Mathf.Sin(angle);

				RaycastHit2D hit = Physics2D.Raycast(_GunPoint.position, dir, _ShootingDistance, _LayerFilter);


				if (hit.collider)
				{
					_LaserHolder.Generate(_GunPoint.position, hit.point);
				 	ShootLaserFromGunPoint(hit.point, hit.transform);
				}
				else
				{
				 	if (_LaserHolder !=  null)
				 	{
				 		Destroy(_LaserHolder.gameObject);
				 		_LaserHolder = null;
				 	}
				}
			}
			else
			{
				if (_LaserHolder !=  null)
				{
					Destroy(_LaserHolder.gameObject);
					_LaserHolder = null;
				}				
			}
		}

		private bool InsideCamera(Vector2 hitPoint)
		{
			Vector2 extents;
			extents.y = _CameraMain.orthographicSize;
			extents.x = extents.y * Screen.width / Screen.height;
			Vector3 cameraPosition = _CameraMain.transform.position;

			Vector2 topLeft = new Vector2(cameraPosition.x - extents.x, cameraPosition.y + extents.y);
			Vector2 bottomRight = new Vector2(cameraPosition.x + extents.x, cameraPosition.y - extents.y);

			bool withinBoundaries = hitPoint.x > topLeft.x && hitPoint.x < bottomRight.x && hitPoint.y < topLeft.y && hitPoint.y > bottomRight.y;

			return withinBoundaries;
		}

		private void ShootLaserFromGunPoint(Vector2 hitPoint, Transform other)
		{
			if (InsideCamera(hitPoint))
			{
				_RayGun.Attack(other);
			}
		}

		private void SetWeapon()
		{
			_RayGun = GameManager.Instance.CurrentRayGun;
		}

		private void PlaySounds()
		{
			if (_PUC.shootDown)
			{
				AudioManager.Instance.PlaySound(_RayGun.StartSound);
				AudioManager.Instance.PlaySound(_RayGun.ShootSound);

				// Spawn particle
				if (_MuzzleParticleHolder == null)
				{
					_MuzzleParticleHolder = Instantiate(_MuzzleParticle, _GunPoint.position, Quaternion.identity);
				}
			}
			if (_PUC.shootReleased)
			{
				AudioManager.Instance.StopSound(_RayGun.ShootSound);
				if (_MuzzleParticleHolder != null)
				{
					Destroy(_MuzzleParticleHolder.gameObject);
					_MuzzleParticleHolder = null;
				}
			}
		}

	#endregion
}
