﻿using System;
using UnityEngine;

// A very simple listener to disable Upgrade Station
public class UpgradeStn : MonoBehaviour
{

	public void StartListening()
	{
		EventManager.StartListening("UpgradeApplied", this.DisableStation);
	}

    private void DisableStation()
    {
		// Destroy all children
		foreach (Transform child in transform) {
			Destroy(child.gameObject);
		}

        EventManager.StopListening("UpgradeApplied", this.DisableStation);
    }
}
