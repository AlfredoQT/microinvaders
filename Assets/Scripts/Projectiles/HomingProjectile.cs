﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Collider2D))]
public class HomingProjectile : MonoBehaviour
{
	#region GlobalVars

		[SerializeField]
		private float _DamageDealt = 20.0f;

		[SerializeField]
		private float _MovementSpeed = 7f;

		[SerializeField]
		private Transform _Target;

		private Rigidbody2D _RB;
		private SpriteRenderer _SR;

		public Transform Target
		{
			set
			{
				_Target = value;
			}
		}

		public float Damage
		{
			set 
			{
				_DamageDealt = value;
			}
		}



	#endregion

	#region UnityMethods

		private void Awake()
		{
			_RB = GetComponent<Rigidbody2D>();
			_SR = GetComponent<SpriteRenderer>();
		}

		private void Start()
		{
			_RB.gravityScale = 0.0f;
		}

		private void Update()
		{
			// Just so you know it's a homming one
			_SR.color = new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), 0.5f);
		}

		private void FixedUpdate()
		{
			// Get the direction to the target
			Vector3 dir = (_Target.position - transform.position).normalized;

			// Move it
			transform.position += dir * _MovementSpeed * Time.deltaTime;
		}

		private void OnCollisionEnter2D(Collision2D other)
		{
			if (other.collider.CompareTag("Player"))
			{
				Health playerHealth = other.collider.GetComponent<Health>();
				if (playerHealth != null)
				{
					playerHealth.ReceiveDamage(_DamageDealt);
				}
				
			}
			Destroy(gameObject);
		}

	#endregion

	#region ProjectileLogic

		public void Shoot(Transform target, float movementSpeed)
		{
			_Target = target;
			_MovementSpeed = movementSpeed;
		}

	#endregion
}
