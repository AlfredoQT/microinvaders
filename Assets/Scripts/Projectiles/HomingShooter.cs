﻿using UnityEngine;

public class HomingShooter : MonoBehaviour
{
	#region GlobalVars

		[Header("Homing Projectile Settings")]

		[SerializeField]
		private Transform _Target;

		[SerializeField]
		private HomingProjectile _Projectile;

		[SerializeField]
		private float _Speed = 20.0f;

		[SerializeField]
		private float _FireDelayMin = 1.5f;

		[SerializeField]
		private float _FireDelayMax = 3.0f;

		private float _TimeLeftToShoot;

	#endregion

	#region LifeCycle

		private void Start()
		{
			_TimeLeftToShoot = 0.0f;
			
		}
		
		private void Update()
		{
			if (_TimeLeftToShoot <= 0.0f)
			{
				ShootProjectile();

				_TimeLeftToShoot = (Random.Range(_FireDelayMin,_FireDelayMax));
			}
			_TimeLeftToShoot = _TimeLeftToShoot > 0.0f ? _TimeLeftToShoot - Time.deltaTime : 0.0f;
		}

    #endregion

	#region ClassMethods

		private void ShootProjectile()
		{

			Instantiate(_Projectile, transform.position, Quaternion.identity)
			.Shoot(_Target, _Speed);
		}

	#endregion
}
