﻿using System.Collections;
using UnityEngine;

public class ManyProjectileShoot : MonoBehaviour
{

	#region GlobalVars

		[SerializeField]
		private Projectile _ProjectilePrefab;

		[SerializeField]
		private float _Speed = 20.0f;

		[SerializeField]
		private float _FireDelayMin = 1.5f;

		[SerializeField]
		private float _FireDelayMax = 2.0f;

		[SerializeField]
		private float _TimeBetweenProjectiles = 0.4f;

		[SerializeField]
		private int _ProjectilesToShoot = 3;

		private float _TimeLeftToShoot;

	#endregion

	#region Shooting

		private void Start()
		{
			_TimeLeftToShoot = 0.0f;
		}

		private void Update()
		{
			_TimeLeftToShoot = _TimeLeftToShoot > 0.0f ? _TimeLeftToShoot - Time.deltaTime : 0.0f;
			if (_TimeLeftToShoot <= 0.0f)
			{
				SpawnProjectiles();

				_TimeLeftToShoot = UnityEngine.Random.Range(_FireDelayMin, _FireDelayMax);
			}
		}

		private void SpawnProjectiles()
		{
			StartCoroutine(Shoot());
			StopCoroutine(Shoot());
		}

		private IEnumerator Shoot()
		{
			int i = 0;
			while (i < _ProjectilesToShoot)
			{
				Instantiate(_ProjectilePrefab, transform.position, Quaternion.identity)
				.Shoot(transform.right * _Speed, 0.0f);
				++i;
				yield return new WaitForSeconds(_TimeBetweenProjectiles);
			}
		}

    #endregion

}
