﻿using UnityEngine;

public class MultiDirectionProjectile : MonoBehaviour
{

	#region GlobalVars

		[Header("Projectile Settings")]

		[SerializeField]
		private Projectile _Projectile;

		[SerializeField]
		private float _Speed;

		[SerializeField]
		private float _FireDelayMin = 1.5f;

		[SerializeField]
		private float _FireDelayMax = 2.0f;

		[SerializeField, Tooltip("Enter in degrees")]
		private float _EnclosingAngle = 45.0f;

		[Tooltip("Enter one if you use only one. Even numbers will become uneven")]
		[Range(1, 7)]
		[SerializeField]
		private int _ProjectilesToShoot = 3;

		private float _TimeLeftToShoot;

	#endregion

	#region LifeCycle

		private void Start()
		{
			_TimeLeftToShoot = 0.0f;
			_ProjectilesToShoot = _ProjectilesToShoot % 2 == 0 ? _ProjectilesToShoot - 1 : _ProjectilesToShoot;
		}
		
		private void Update()
		{
			if (_TimeLeftToShoot <= 0.0f)
			{
				ShootProjectiles();

				_TimeLeftToShoot = (Random.Range(_FireDelayMin,_FireDelayMax));
			}
			_TimeLeftToShoot = _TimeLeftToShoot > 0.0f ? _TimeLeftToShoot - Time.deltaTime : 0.0f;
		}

    #endregion

	#region ClassMethods

		private void ShootProjectiles()
		{
			float angleRed = Mathf.Atan2(transform.right.y, transform.right.x);

			Instantiate(_Projectile, transform.position, Quaternion.identity)
			.Shoot(transform.right * _Speed, 0.0f);

			if ( _ProjectilesToShoot  > 1)
			{
				float minimumAngle = (_EnclosingAngle * Mathf.Deg2Rad) / (_ProjectilesToShoot / 2);

				// Say 7, 2, 0..2
				for (int i = 0; i < _ProjectilesToShoot / 2; i++)
				{
					float angleToShoot = minimumAngle * (i + 1);

					Instantiate(_Projectile, transform.position, Quaternion.identity)
					.Shoot(new Vector2(Mathf.Cos(angleRed + angleToShoot),
					Mathf.Sin(angleRed + angleToShoot)) * _Speed, 0.0f);

					Instantiate(_Projectile, transform.position, Quaternion.identity)
					.Shoot(new Vector2(Mathf.Cos(angleRed - angleToShoot),
					Mathf.Sin(angleRed - angleToShoot)) * _Speed, 0.0f);
				}
			}
		}

	#endregion

}
