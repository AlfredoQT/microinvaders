﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public class Projectile : MonoBehaviour
{

    #region GlobalVars

    [SerializeField]
    private float _DamageDealt = 20.0f;

	[SerializeField]
	private AudioSource _SimplePlayer;

	[SerializeField]
	private Sound _SoundOnDestroy;

    private Rigidbody2D _RB;

    #endregion

    #region UnityMethods

    private void Awake()
    {
        _RB = GetComponent<Rigidbody2D>();
    }

	private void Start()
	{
		if (_SimplePlayer != null)
		{
			if (_SoundOnDestroy !=  null)
			{
				_SimplePlayer.clip = _SoundOnDestroy.clip;
				_SimplePlayer.loop = _SoundOnDestroy.loop;
				_SimplePlayer.spatialBlend = _SoundOnDestroy.SpatialBlend;
				_SimplePlayer.pitch = _SoundOnDestroy.pitch;
				_SimplePlayer.volume = _SoundOnDestroy.volume;
				_SimplePlayer.maxDistance = _SoundOnDestroy.MaxDistance;
				_SimplePlayer.minDistance = _SoundOnDestroy.MinDistance; 
			}
		}
	}

    private void OnCollisionEnter2D(Collision2D other)
    {
		if (_SimplePlayer != null)
		{
			Instantiate(_SimplePlayer, transform.position, Quaternion.identity);
		}
        if (other.collider.CompareTag("Player"))
        {
            Health playerHealth = other.collider.GetComponent<Health>();
            if (playerHealth != null)
            {
                playerHealth.ReceiveDamage(_DamageDealt);
            }

        }
        Destroy(gameObject);
    }

    #endregion

    #region ProjectileLogic

    public void Shoot(Vector2 velocity, float gravityScale)
    {
        _RB.velocity = velocity;
        _RB.gravityScale = gravityScale;
    }

    public void SetDamage(float damage)
    {
        _DamageDealt = damage;
    }

    #endregion

}
