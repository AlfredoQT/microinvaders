﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prologue : MonoBehaviour
{


    [SerializeField]
    private Timer _Timer;

    private bool _Skip = false;

    private void Start()
    {
		Timer NewTimer = new Timer();
        EventManager.TriggerEvent("PausedGame");
    }

    private void Update()
    {
        if (Input.anyKeyDown)
        {
            _Skip = true;
        }

        if (_Skip)
        {
			_Timer.TimeLeft = 0;
        }
    }

}
