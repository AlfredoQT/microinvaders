﻿using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Animator))]
public class BossUI : MonoBehaviour
{

	private Animator _Anim;

	private void Awake()
	{
		_Anim = GetComponent<Animator>();
	}

	private void Start()
	{
		EventManager.StartListening("BossDefeated", PrepareToFade);
	}

	private void PrepareToFade()
	{
		_Anim.SetBool("FadeIn", true);
		AnimatorClipInfo[] currentAnimClipInfo = _Anim.GetCurrentAnimatorClipInfo(0);
		Invoke("LoadScene", currentAnimClipInfo[0].clip.length);
	}

	private void LoadScene()
	{
		SceneManager.LoadScene("VictoryScene");
	}

}
