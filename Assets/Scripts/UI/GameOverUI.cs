﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverUI : MonoBehaviour 
{

    private void Start()
    {
        AudioManager.Instance.PlaySound("GameOverTheme");
    }
     
    public void OnPressedMainMenu()
    {
        AudioManager.Instance.StopSound("GameOverTheme");
        SceneManager.LoadScene("MainMenu");
    }

}
