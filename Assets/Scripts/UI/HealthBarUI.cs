﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HealthBarUI : MonoBehaviour
{

	[SerializeField]
	private Health _PlayerHealth;

	[SerializeField]
	private Image _HealthSquare;

	[SerializeField]
	private Vector3 firstSquareOffset = new Vector3(-100.0f, 0.0f, 0.0f);

	private GameObject _HealthBar;

	private void Start()
	{
		_HealthBar = transform.Find("HealthBar").gameObject;
	}

	private void Update()
	{
		// Empty the health bar
		for (int i = 0; i < _HealthBar.transform.childCount; ++i)
		{
			GameObject toDestroy = _HealthBar.transform.GetChild(i).gameObject;
			Destroy(toDestroy);
		}

		// Add them back
		for (int i = 0; i < healthSquaresToDraw(); ++i)
		{
			Image healthSquare = Instantiate(_HealthSquare);
			healthSquare.transform.SetParent(_HealthBar.transform);
			RectTransform rectTransform = healthSquare.GetComponent<RectTransform>();
			Vector3 newPosition = new Vector3(52.0f * i, 0.0f, 0.0f);
			rectTransform.localPosition = firstSquareOffset + newPosition;
		}
	}

	private int healthSquaresToDraw()
	{
		float healthRatio = _PlayerHealth.CurrentHealth / _PlayerHealth.MaxHealth;
		if (healthRatio <= 0.0f)
		{
			SceneManager.LoadScene("GameOver");
			Debug.Log("GameOver");
			return 0;
		}
		else if (healthRatio <= 0.2f)
		{
			return 1;
		}
		else if (healthRatio <= 0.4f)
		{
			return 2;
		}
		else if (healthRatio <= 0.6f)
		{
			return 3;
		}
		else if (healthRatio <= 0.8f)
		{
			return 4;
		}
		else
		{
			return 5;
		}
	}
}
