﻿using UnityEngine;
using UnityEngine.UI;

public class InGameUI : MonoBehaviour
{

	#region UIElements

		[SerializeField]
		private Text _GemCountText;

		[SerializeField]
		private PlayerHealth _PlayerHealth;

		[SerializeField]
		private Scrollbar _HealthScrollbar;

		[SerializeField]
		private Scrollbar _ExtraHealthScrollbar;

		[SerializeField]
		private GameObject _ExtraHealthContainer;

		[SerializeField]
		private Image _WideImage;

		[SerializeField]
		private Image _FreezeImage;

		[SerializeField]
		private Text _LiveCountText;

	#endregion

	#region LifeCycle

		private void Update()
		{
			_GemCountText.text = "x " + GameManager.Instance.GemCount;
			_HealthScrollbar.size = _PlayerHealth.CurrentHealth / _PlayerHealth.MaxHealth;
			_ExtraHealthContainer.SetActive(GameManager.Instance.HealthUpgradePurchased);
			_LiveCountText.text = "x " + GameManager.Instance.Lives;

			if (_ExtraHealthContainer.activeInHierarchy)
			{
				_ExtraHealthScrollbar.size = _PlayerHealth.CurrentExtraHealth / _PlayerHealth.MaxExtraHealth;
			}

			if (_WideImage != null)
			{
				_WideImage.gameObject.SetActive(GameManager.Instance.WideBeamPurchased);
			}
			if (_FreezeImage != null)
			{
				_FreezeImage.gameObject.SetActive(GameManager.Instance.FreezeBeamPurchased);
			}
			
			
		}

	#endregion

}
