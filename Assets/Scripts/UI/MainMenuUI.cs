﻿using UnityEngine;
using UnityEngine.SceneManagement;


public class MainMenuUI : MonoBehaviour
{

	[SerializeField]
	private GameObject _MainUI;

	[SerializeField]
	private GameObject _ControlsUI;

	[SerializeField]
	private GameObject _CreditsUI;

	void Start()
	{
		Cursor.lockState = CursorLockMode.None;
		AudioManager.Instance.PlaySound("MainMenuTheme");
	}

	public void OnPressedStartGame()
	{
		AudioManager.Instance.StopSound("MainMenuTheme");
		SceneManager.LoadScene("Prologue");
		
	}

	public void Update()
	{
		if (Input.GetButtonDown("Cancel"))
		{
			OnPressedBack();
		}
		
	}
	
	public void OnPressedControls()
	{
		_MainUI.SetActive(false);
		_ControlsUI.SetActive(true);
		_CreditsUI.SetActive(false);
	}

	public void OnPressedCredits()
	{
		_MainUI.SetActive(false);
		_ControlsUI.SetActive(false);
		_CreditsUI.SetActive(true);
	}

	public void OnPressedBack()
	{
		_MainUI.SetActive(true);
		_ControlsUI.SetActive(false);
		_CreditsUI.SetActive(false);
	}
}
