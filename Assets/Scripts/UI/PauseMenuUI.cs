﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Animator))]
public class PauseMenuUI : MonoBehaviour 
{
	[SerializeField]
	private AudioSource _LevelTheme;

	private Animator _Anim;
	private bool _Paused = false;

	private void Awake()
	{
		_Anim = GetComponent<Animator>();
	}
	
	private void Update()
	{
		if(Input.GetKeyDown(KeyCode.P))
		{
			if(_Paused)
			{
				Resume();
			}
			else
			{
				Pause();
			}
		}
		_Anim.SetBool("Show", _Paused);
	}

	public void Resume()
	{
		EventManager.TriggerEvent("ResumedGame");
		_Paused = false;
		AudioManager.Instance.StopSound("PauseMenuTheme");
		_LevelTheme.Play();
		Time.timeScale = 1.0f;
	}
	
	private void Pause()
	{
		EventManager.TriggerEvent("PausedGame");
		_Paused = true;
		AudioManager.Instance.PlaySound("PauseMenuTheme");
		_LevelTheme.Stop();
		Time.timeScale = 0.0f;
	}

	public void OnLoadMenu()
	{
		EventManager.TriggerEvent("ResumedGame");
		_Paused = false;
		AudioManager.Instance.StopSound("PauseMenuTheme");
		SceneManager.LoadScene("MainMenu");
		Time.timeScale = 1.0f;
	}
	
}
