﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

[RequireComponent(typeof(Animator))]
public class UpgradeStationUI : MonoBehaviour
{

    #region Serializables

    [SerializeField]
    private Text _TotalGems;

    [SerializeField]
    private Text _HealthUpgradeText;

    [SerializeField]
    private Text _WideUpgradeText;

    [SerializeField]
    private Text _FreezeUpgradeText;

    [SerializeField]
    private UnityEvent _OnUpgradeDone;


    #endregion

    #region Members

    private Animator _Anim;

    #endregion

    #region LifeCycle

    private void Awake()
    {
        _Anim = GetComponent<Animator>();
    }

    private void Start()
    {
        _HealthUpgradeText.text = GameManager.Instance.HealthUpgradeCost.ToString();
        _WideUpgradeText.text = GameManager.Instance.WideBeamUpgradeCost.ToString();
        _FreezeUpgradeText.text = GameManager.Instance.FreezeBeamUpgradeCost.ToString();
    }

    private void Update()
    {
        _TotalGems.text = "Your Gems: " + GameManager.Instance.GemCount;
    }

    #endregion

    #region Events

    public void PurchaseHealthUpgrade()
    {
        if (!GameManager.Instance.HealthUpgradePurchased && GameManager.Instance.GemCount >= GameManager.Instance.HealthUpgradeCost)
        {
            _Anim.SetBool("Show", false);
            AudioManager.Instance.PlaySound("UpgradeStationApplying");
            Invoke("OnUpgradeDone", AudioManager.Instance.GetSoundLength("UpgradeStationApplying"));
            GameManager.Instance.HealthUpgradePurchased = true;
            GameManager.Instance.GemCount -= GameManager.Instance.HealthUpgradeCost;
            EventManager.TriggerEvent("HealthUpgradeApplied");
        }
        else
        {
			AudioManager.Instance.PlaySound("UpgradeStationDeny");
        }
    }

    public void PurchaseWideBeam()
    {
        if (!GameManager.Instance.WideBeamPurchased && GameManager.Instance.GemCount >= GameManager.Instance.WideBeamUpgradeCost)
        {
            _Anim.SetBool("Show", false);
            AudioManager.Instance.PlaySound("UpgradeStationApplying");
            Invoke("OnUpgradeDone", AudioManager.Instance.GetSoundLength("UpgradeStationApplying"));
            GameManager.Instance.WideBeamPurchased = true;
            GameManager.Instance.GemCount -= GameManager.Instance.WideBeamUpgradeCost;
            GameManager.Instance.CurrentRayGun.AddUpgrade(new WideRaygunUpgrade());
        }
		        else
        {
			AudioManager.Instance.PlaySound("UpgradeStationDeny");
        }
    }

    public void PurchaseFreezeBeam()
    {
        if (!GameManager.Instance.FreezeBeamPurchased && GameManager.Instance.GemCount >= GameManager.Instance.FreezeBeamUpgradeCost)
        {
            _Anim.SetBool("Show", false);
            AudioManager.Instance.PlaySound("UpgradeStationApplying");
            Invoke("OnUpgradeDone", AudioManager.Instance.GetSoundLength("UpgradeStationApplying"));
            GameManager.Instance.FreezeBeamPurchased = true;
            GameManager.Instance.GemCount -= GameManager.Instance.FreezeBeamUpgradeCost;
            GameManager.Instance.CurrentRayGun.AddUpgrade(new FreezeRaygunUpgrade());
        }
		        else
        {
			AudioManager.Instance.PlaySound("UpgradeStationDeny");
        }
    }

    public void Open()
    {
        _Anim.SetBool("Show", true);
    }

    public void Close()
    {
        _Anim.SetBool("Show", false);
    }

    private void OnUpgradeDone()
    {
        EventManager.TriggerEvent("UpgradeApplied");
        AudioManager.Instance.PlaySound("UpgradeStationDone");
        _OnUpgradeDone.Invoke();
    }

    #endregion

}
