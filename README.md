# Micro Invaders

## Summary

A 2D platformer game built using Unity.

## Features

* Weapon Upgrades
* 8 directional shooting. Like Contra
* Mario like Jumping
* Featuring two levels

## Special Thanks

### Willy Campos

Unity Instructor at VFS

### Scott Henshaw

Web Development instructor at VFS

### Steve Royea

Sound Designer at VFS

### Kevin Mcleod

Music for our game. Incompetech

## Contributors

### Artur Nakauth

Project Manager
Sound Designer

### Alfredo Quintero Tlacuilo

Core Gameplay Programmer

### Kinjal Abhani

UI programmer

### Rodrigo Naufal

Level Designer

### Varun Sonti

2D Artist
